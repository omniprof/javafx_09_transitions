package com.kenfogel.javafx_09_transitions;

import java.util.List;
import javafx.animation.PathTransition;
import javafx.animation.Timeline;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.CubicCurveTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.Shape;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * Example of transitions that can create animation
 *
 * @author Ken
 */
public class MainApp extends Application {

    /**
     * Start the JavaFX application
     *
     * @param stage Primary stage.
     * @throws Exception Exception thrown during application.
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        final Pane root = new Pane();
        final Scene scene = new Scene(root, 600, 400, Color.GHOSTWHITE);
        primaryStage.setScene(scene);
        primaryStage.setTitle("JavaFX 2 Animations");
        primaryStage.show();
        applyAnimation(root);
    }

    /**
     * Generate Path upon which animation will occur.
     *
     * @param pathOpacity The opacity of the path representation.
     * @return Generated path.
     */
    private Path generateCurvyPath(double pathOpacity) {
        Path path = new Path();
        path.getElements().add(new MoveTo(20, 20));
        path.getElements().add(new CubicCurveTo(380, 0, 380, 120, 200, 120));
        path.getElements().add(new CubicCurveTo(0, 120, 0, 240, 380, 240));
        path.setOpacity(pathOpacity);
        return path;
    }

    /**
     * Generate the path transition.
     *
     * @param shape Shape to travel along path.
     * @param path Path to be traveled upon.
     * @return PathTransition.
     */
    private PathTransition generatePathTransition(Shape shape, Path path) {
        PathTransition pathTransition = new PathTransition();
        pathTransition.setDuration(Duration.seconds(8.0));
        pathTransition.setDelay(Duration.seconds(2.0));
        pathTransition.setPath(path);
        pathTransition.setNode(shape);
        pathTransition.setOrientation(PathTransition.OrientationType.ORTHOGONAL_TO_TANGENT);
        pathTransition.setCycleCount(Timeline.INDEFINITE);
        pathTransition.setAutoReverse(true);
        return pathTransition;
    }

    /**
     * Determine the path's opacity based on command-line argument if supplied
     * or zero by default if no numeric value provided.
     *
     * @return Opacity to use for path.
     */
    private double determinePathOpacity() {
        Parameters params = getParameters();
        List<String> parameters = params.getRaw();
        double pathOpacity = 0.0;
        if (!parameters.isEmpty()) {
            try {
                pathOpacity = Double.valueOf(parameters.get(0));
            } catch (NumberFormatException nfe) {
                pathOpacity = 0.0;
            }
        }
        return pathOpacity;
    }

    /**
     * Apply animation, the subject of this class.
     *
     * @param group Group to which animation is applied.
     */
    private void applyAnimation(Pane pane) {
        Circle circle = new Circle(20, 20, 15);
        circle.setFill(Color.DARKRED);
        Path path = generateCurvyPath(determinePathOpacity());
        pane.getChildren().add(path);
        pane.getChildren().add(circle);
        pane.getChildren().add(new Circle(20, 20, 5));
        pane.getChildren().add(new Circle(380, 240, 5));
        PathTransition transition = generatePathTransition(circle, path);
        transition.play();
    }

    /**
     * Main function for running JavaFX application.
     *
     * @param arguments Command-line arguments; optional first argument is the
     * opacity of the path to be displayed (0 effectively renders path
     * invisible).
     */
    public static void main(final String[] arguments) {
        launch(arguments);
    }
}
